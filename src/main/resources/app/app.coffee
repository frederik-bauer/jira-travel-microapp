define (require)->

  config                  = require 'config'
  helpers                 = require 'helpers'

  UserModel               = require 'models/user_model'
  CurrentUserModel        = require 'models/current_user_model'
  ProjectModel            = require 'models/project_model'
  SavedTripModel          = require 'models/saved_trip_model'
  TripModel               = require 'models/trip_model'

  CustomFieldsCollection  = require 'collections/custom_fields_collection'
  IssueTypesCollection    = require 'collections/issue_types_collection'
  TripsCollection         = require 'collections/trips_collection'
  UsersCollection         = require 'collections/users_collection'

  AddTripView             = require 'views/add_trip_view'
  FlashView               = require 'views/flash_view'
  MyTripView              = require 'views/my_trip_view'
  MyTripsView             = require 'views/my_trips_view'
  PendingTripsView        = require 'views/pending_trips_view'

  Router                  = require 'router'
  
  # config needs to be global because it's referenced templates
  window.config = config

  currentUser = new CurrentUserModel
  trips = new TripsCollection
  project = new ProjectModel
  issuetype = new IssueTypesCollection
  customFields = new CustomFieldsCollection

  addTripView = new AddTripView 
    collection: trips

  trips.fetch().done ->
    myTripsView = new MyTripsView 
      collection: trips

    pendingTripsView = new PendingTripsView 
      collection: trips

  appRouter = new Router
    addTripView: addTripView

  Backbone.history.start()