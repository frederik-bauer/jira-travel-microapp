define (require)->

  config = require '../config'
    
  class SavedTripModel extends Backbone.Model
    url: =>
      "#{config.restPath}/api/2/issue/#{@key}"

    initialize: (options) ->
      @key = if options and options.key then options.key else ""
      if @key
        @fetch(options).done (d) =>
          options.callback(d) if options.callback

    parse: (d) ->
      if d.fields
        _.extend d.fields,
          id: d.id
          key: d.key
      else
        d