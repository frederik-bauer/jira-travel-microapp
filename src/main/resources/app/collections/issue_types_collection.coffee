define (require)->

  config = require '../config'

  class IssueTypesCollection extends Backbone.Collection
    url: "#{config.restPath}/api/2/issuetype"
    initialize: ->
      @fetch().done @findTravelReqIssueType
    findTravelReqIssueType: =>
      issueType = @find (it) ->
        it.attributes.name == config.issueTypeName
      @id = issueType.attributes.id
