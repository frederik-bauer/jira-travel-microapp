define (require)->

  config    = require '../config'
  TripModel = require '../models/trip_model'

  class TripsCollection extends Backbone.Collection
    url: "#{config.restPath}/api/2/search"

    model: TripModel

    comparator: (a,b) =>
      a.get(config.customFields.departDate.id) < b.get(config.customFields.returnDate.id)

    parse: (d) ->
      _.map d.issues, (i) =>
        _.extend i.fields,
          id: i.id
          key: i.key